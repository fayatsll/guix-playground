CC = gcc
CFLAGS = -Wall

main: main.c
	@echo CC is $(CC)
	$(CC) $(CFLAGS) main.c -o build/main
