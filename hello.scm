(define-module (hello))
(use-modules (ice-9 match))

(+ 1 2)

(define (comment i) '())

(comment
  (+ 1 1) ; 2
  (* 2 2))
  

(define (salute2 name)
  `("hello" ,name))

(salute2 "alice") ; ("hello" "alice")

(define-public salute
  (lambda () (display "hello World\n")))

(define-public double
  (lambda (x) (* 2 x)))

(define-public neg!
  (lambda (b) (cond ((eq? #t b) #f)
                   (else #t))))

(define-public (neg!! bool)
  "Inverts a boolean"
  (cond ((eq? #t bool) #f)
        (else #t))) ; ; Empty result



(define-public (is-even? n)
  (if (eq? (modulo n 2) 0)
      #t
      #f))
