(define-module (no-program manifest))

(use-modules (gnu packages)
             (gnu packages base)
             (guix)
             (guix packages)
             (guix build-system copy)
             ;; (guix build-system trivial)
             (guix gexp)
             (guix licenses))
 

(define gtest
  #~(begin
      (mkdir #$output)

      (system* (string-append #+coreutils "/bin/touch") (string-append #$output "/hello"))

      (symlink #$(plain-file "foo" "bar") (string-append #$output "/foo"))

      (symlink #$(file-append coreutils "/bin/touch") (string-append #$output "/touch"))))

gtest

(define m-gtest
  (gexp->derivation "gtest" gtest))

m-gtest

(define cf-gtest
  (computed-file "gtest" gtest))

cf-gtest



(define symlink-irssi
  (gexp->derivation "link-to-irssi"
    #~(symlink #$(file-append irssi "/bin/irssi") #$output)))

symlink-irssi ; #<procedure 7f41d1ca8870 at guix/gexp.scm:1201:2 (state)>

(define symlink-irssi-drv
  (with-store store
    (run-with-store store
      symlink-irssi)))

symlink-irssi-drv ; #<derivation /gnu/store/qhllrdn5wzwfb509xvmiza0hggiwb8a1-link-to-irssi.drv => /gnu/store/blfrz2wlz7kcv6aj8ml6fcjr2iiibz3p-link-to-irssi 7f41cf9145a0>

(define-public cf
  (computed-file "my-file"
                 #~(begin
                     (mkdir #$output)
                     (system* "touch"
                              (string-append #$output "/file")))))


(define hosts
  (local-file "/etc/hosts"))

(define bar "foo")

(define empty
  #~(begin
      (mkdir #$output)
      (system* (file-append #$coreutils "/bin/touch") (string-append #$output "/file"))))

empty

(define build-exp
  #~(begin
      (mkdir #$output)
      (chdir #$output)
      (symlink (string-append #$coreutils "/bin/ls")
               "list-files"))) 
 

(define-public p
  (package
    (name "no-program")
    (version "0.0")

    (source
     (local-file "file"))

    (build-system copy-build-system)

    (description "")
    (home-page "")
    (license gpl3+)
    (synopsis "")))

