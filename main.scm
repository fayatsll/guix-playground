(use-modules (hello))

(display "hello")

(salute)
; hello World
; 

(display
  (double 2))

(double (double (double 2))) ; 16

(neg! #f) ; #t
