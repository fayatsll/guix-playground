(use-modules (gnu home)
             (gnu packages)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages package-management)
             (gnu packages gnupg))

(home-environment
 (packages
  (list
   guile-3.0
   guile-readline
   guile-colorized
   guile-gcrypt
   glibc-locales
   )))

